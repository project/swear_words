<?php

namespace Drupal\swear_words\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure swear_words settings words.
 */
class WordSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'swear_words_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['swear_words.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('swear_words.settings');

    $form['comment_body'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment body machine name'),
      '#default_value' => $config->get('body'),
      '#prefix' => '<div id="body-text">',
      '#description' => $this->t("Specify the comment field machine name that contains the message, by default it is comment_body."),
      '#suffix' => '</div>',
    ];

    $form['words'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Not allowed words/phrases'),
      '#default_value' => $config->get('words'),
      '#prefix' => '<div id="word-text">',
      '#description' => $this->t("Introduce the words/phrases separated by a comma to not publish comments if contains some of this words,
       don't use spaces between words/phrases, the words are case insensitive. E.g. Word1,Word2,Word3"),
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('swear_words.settings')
      ->set('body', $values['comment_body'])
      ->set('words', $values['words'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
