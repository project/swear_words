CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

The Swear Words is a simply module that checks the body of the comments allowing the admins
of the site specify certain words that won't allow the anonymous/registered user publish 
the comment if it contains some one the words/phrases we specify in the config form.


INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Install the core module Comment.
    
 2. Copy/upload the module to the modules directory of your Drupal
   installation.

 3. Enable the module and create a comment type. 

 4. Set up the settings and the words con the config form.
 (/admin/config/content/swear-words)


CONFIGURATION
-------------

 * Build a new comment type and create a new comment body or use the provided by default.
   
 * Specify on the config form the field body machine name that your comments will use
 (by default "comments_body") and write the words/phrases desired to deny the user auto-
 publish the comment if contains some of this.
 